import createCanvas from "../../src/component/canvas";
import { Canvas, Size } from "../../src/types";

// @TODO: Add tests!

beforeEach(() => {
    jest.clearAllMocks();
});

describe("canvas", () => {
    test("it should return a function", () => {
        const mockHtmlCanvas = {} as unknown as HTMLCanvasElement;
        expect(typeof createCanvas(mockHtmlCanvas)).toBe("function");
    });
});

describe("canvas: setSize", () => {
    test("it should call setObject once", () => {
        const utils = require("../../src/utils");
        // tslint:disable-next-line
        utils.setObject = jest.fn();
        const mockCanvas = {} as unknown as HTMLCanvasElement;
        const result: Canvas = createCanvas(mockCanvas)();
        const mockSize = { width: 10, height: 20 } as unknown as Size;
        result.setSize(mockSize);
        expect(utils.setObject).toBeCalledTimes(1);
    });
});

describe("canvas: getSize", () => {
    test("it should return size with html canvas width and height", () => {
        const mockCanvas = {
            width: 10,
            height: 20
        } as unknown as HTMLCanvasElement;
        const result: Canvas = createCanvas(mockCanvas)();
        expect(result.getSize()).toEqual({ width: 10, height: 20 });
    });
});

describe("canvas: getContext", () => {
    test("it should return canvas context if it exists on html canvas", () => {
        const mockHtmlContext = {} as unknown as CanvasRenderingContext2D;
        const getContext = jest.fn().mockReturnValue(mockHtmlContext);
        const mockCanvas = {
            getContext
        } as unknown as HTMLCanvasElement;
        const result: Canvas = createCanvas(mockCanvas)();
        expect(typeof result.getContext()).toBe("object"); // @TODO: make better
    });
});

describe("canvas: getBounds", () => {
    test("it should call getBoundingClientRect", () => {
        const getBoundingClientRect = jest.fn();
        const mockCanvas = {
            getBoundingClientRect
        } as unknown as HTMLCanvasElement;
        const result: Canvas = createCanvas(mockCanvas)();
        result.getBounds();
        expect(getBoundingClientRect).toBeCalledTimes(1);
    });

    test("it should return html canvas bounds", () => {
        const mockReturnValue = "mock!";
        const getBoundingClientRect = jest.fn().mockReturnValue(mockReturnValue);
        const mockCanvas = {
            getBoundingClientRect
        } as unknown as HTMLCanvasElement;
        const result: Canvas = createCanvas(mockCanvas)();
        expect(result.getBounds()).toBe(mockReturnValue);
    });
});
