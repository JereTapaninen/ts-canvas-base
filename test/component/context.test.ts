import createContext from "../../src/component/context";
import { Context, Rectangle, Point } from "../../src/types";
import { FillStyle, Font } from "../../src/constants";

// @TODO: Add tests!

beforeEach(() => {
    jest.clearAllMocks();
});

describe("context", () => {
    test("it should return a function", () => {
        const mockHtmlContext = {} as unknown as CanvasRenderingContext2D;
        expect(typeof createContext(mockHtmlContext)).toBe("function");
    });
});

describe("context: fillRectangle", () => {
    test("it should call setContext once", () => {
        const utils = require("../../src/utils");
        // tslint:disable-next-line
        utils.setContext = jest.fn();
        const fillRect = jest.fn();
        const mockHtmlContext = {
            fillRect
        } as unknown as CanvasRenderingContext2D;
        const result: Context = createContext(mockHtmlContext)();
        const mockFillStyle = "black" as unknown as FillStyle;
        const mockBounds = { x: 0, y: 0, width: 0, height: 0 } as unknown as Rectangle;
        result.fillRectangle(mockFillStyle, mockBounds);
        expect(utils.setContext).toBeCalledTimes(1);
    });

    test("it should call fillRect once", () => {
        const fillRect = jest.fn();
        const mockHtmlContext = {
            fillRect
        } as unknown as CanvasRenderingContext2D;
        const result: Context = createContext(mockHtmlContext)();
        const mockFillStyle = "black" as unknown as FillStyle;
        const mockBounds = { x: 0, y: 0, width: 0, height: 0 } as unknown as Rectangle;
        result.fillRectangle(mockFillStyle, mockBounds);
        expect(fillRect).toBeCalledTimes(1);
    });
});

describe("context: fillText", () => {
    test("it should call setContext once", () => {
        const utils = require("../../src/utils");
        // tslint:disable-next-line
        utils.setContext = jest.fn();
        const fillText = jest.fn();
        const mockHtmlContext = {
            fillText
        } as unknown as CanvasRenderingContext2D;
        const result: Context = createContext(mockHtmlContext)();
        const mockFillStyle = "black" as unknown as FillStyle;
        const mockFont = "24px Roboto" as unknown as Font;
        const mockText = "Hello world!";
        const mockPoint = { x: 0, y: 0 } as unknown as Point;
        result.fillText(mockFillStyle, mockFont, mockText, mockPoint);
        expect(utils.setContext).toBeCalledTimes(1);
    });

    test("it should call fillText once", () => {
        const utils = require("../../src/utils");
        // tslint:disable-next-line
        utils.setContext = jest.fn();
        const fillText = jest.fn();
        const mockHtmlContext = {
            fillText
        } as unknown as CanvasRenderingContext2D;
        const result: Context = createContext(mockHtmlContext)();
        const mockFillStyle = "black" as unknown as FillStyle;
        const mockFont = "24px Roboto" as unknown as Font;
        const mockText = "Hello world!";
        const mockPoint = { x: 0, y: 0 } as unknown as Point;
        result.fillText(mockFillStyle, mockFont, mockText, mockPoint);
        expect(fillText).toBeCalledTimes(1);
    });
});
