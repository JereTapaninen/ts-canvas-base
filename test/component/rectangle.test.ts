import createRectangle from "../../src/component/rectangle";
import { Point, Size } from "../../src/types";

// @TODO: Add tests!

beforeEach(() => {
    jest.clearAllMocks();
});

describe("rectangle", () => {
    test("it should return rectangle object with point and size", () => {
        const mockPoint = { x: 0, y: 10 } as Point;
        const mockSize = { width: 20, height: 40 } as Size;
        const result = createRectangle(mockPoint, mockSize);
        expect(result.x).toBe(mockPoint.x);
        expect(result.y).toBe(mockPoint.y);
        expect(result.width).toBe(mockSize.width);
        expect(result.height).toBe(mockSize.height);
    });
});
