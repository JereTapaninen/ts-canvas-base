import { forTests } from "../../src/drawing/fps";

// @TODO: Add tests!

beforeEach(() => {
    jest.clearAllMocks();
});

describe("setFps", () => {
    test("it should call setObject once", () => {
        const utils = require("../../src/utils");
        // tslint:disable-next-line
        utils.setObject = jest.fn();
        forTests.setFps();
        expect(utils.setObject).toBeCalledTimes(1);
    });
});
