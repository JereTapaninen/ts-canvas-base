import * as utils from "../src/utils";
import { ErrorMessage } from "../src/constants";

beforeEach(() => {
    jest.clearAllMocks();
});

describe("tryDo", () => {
    test("it returns variable if it's not null or undefined", () => {
        const mockAction = 10;
        const mockError = "";
        expect(utils.tryDo<number>(mockAction, mockError)).toBe(mockAction);
    });

    test("it throws error if variable is null", () => {
        const mockAction = null;
        const mockError = "Mock Error!";
        expect(() => utils.tryDo<number>(mockAction, mockError)).toThrowError(mockError);
    });

    test("it throws error if variable is undefined", () => {
        const mockAction = undefined;
        const mockError = "Mock Error!";
        expect(() => utils.tryDo<number>(mockAction, mockError)).toThrowError(mockError);
    });

    test("it throws default error if variable is undefined and error is undefined", () => {
        const mockAction = undefined;
        expect(() => utils.tryDo<number>(mockAction)).toThrowError(ErrorMessage.Default);
    });
});

describe("orThrow", () => {
    test("it returns result from function if two checks are equal", () => {
        const mockCheck1 = true;
        const mockCheck2 = true;
        const mockResult = "Hello world!";
        const mockFunction = () => mockResult;
        const mockError = "";
        expect(utils.orThrow(mockCheck1, mockCheck2, mockFunction, mockError)).toBe(mockResult);
    });

    test("it throws error if two checks are not equal", () => {
        const mockCheck1 = true;
        const mockCheck2 = false;
        const mockResult = "!";
        const mockFunction = () => mockResult;
        const mockError = "Mock Error!";
        expect(() => utils.orThrow(mockCheck1, mockCheck2, mockFunction, mockError))
            .toThrowError(mockError);
    });

    test("it throws default error if two checks are not equal and error is undefined", () => {
        const mockCheck1 = true;
        const mockCheck2 = false;
        const mockResult = "!";
        const mockFunction = () => mockResult;
        expect(() => utils.orThrow(mockCheck1, mockCheck2, mockFunction))
            .toThrowError(ErrorMessage.Default);
    });
});

describe("trueOrThrow", () => {
    test("it returns result from function if boolcheck is true", () => {
        const check = true;
        const mockResult = "Hello world!";
        const mockFunction = () => mockResult;
        const mockError = "";
        expect(utils.trueOrThrow(check, mockFunction, mockError)).toBe(mockResult);
    });

    test("it throws error if check is not true", () => {
        const check = false;
        const mockResult = "!";
        const mockFunction = () => mockResult;
        const mockError = "Mock Error!";
        expect(() => utils.trueOrThrow(check, mockFunction, mockError))
            .toThrowError(mockError);
    });

    test("it throws default error if check is not true and error is undefined", () => {
        const check = false;
        const mockResult = "!";
        const mockFunction = () => mockResult;
        expect(() => utils.trueOrThrow(check, mockFunction))
            .toThrowError(ErrorMessage.Default);
    });
});

describe("doIf", () => {
    test("it calls function once if checks are equal", () => {
        const mockCheck1 = true;
        const mockCheck2 = true;
        const mockFunction = jest.fn();
        utils.doIf(mockCheck1, mockCheck2, mockFunction);
        expect(mockFunction).toBeCalledTimes(1);
    });

    test("it does not call function if checks are not equal", () => {
        const mockCheck1 = true;
        const mockCheck2 = false;
        const mockFunction = jest.fn();
        utils.doIf(mockCheck1, mockCheck2, mockFunction);
        expect(mockFunction).not.toBeCalled();
    });
});

describe("doIfTrue", () => {
    test("it calls function once if check is true", () => {
        const check = true;
        const mockFunction = jest.fn();
        utils.doIfTrue(check, mockFunction);
        expect(mockFunction).toBeCalledTimes(1);
    });

    test("it does not call function if check is not true", () => {
        const check = false;
        const mockFunction = jest.fn();
        utils.doIfTrue(check, mockFunction);
        expect(mockFunction).not.toBeCalled();
    });
});

describe("objectContains", () => {
    test("it should return true if object contains all passed in keys", () => {
        const mockObject = { hello: "world", world: "hello" };
        const mockKeys: ReadonlyArray<string> = [ "hello", "world" ];
        expect(utils.objectContains(mockObject, ...mockKeys)).toBe(true);
    });

    test("it should return false if object does not contain all passed in keys", () => {
        const mockObject = { hello: "world", world: "hello" };
        const mockKeys: ReadonlyArray<string> = [ "hello", "aaa" ];
        expect(utils.objectContains(mockObject, ...mockKeys)).toBe(false);
    });
});

describe("getCanvasFromHTML", () => {
    test("it should return canvas if it's found from HTML", () => {
        const mockReturnValue = "hello world!";
        // tslint:disable-next-line
        document.getElementById = jest.fn().mockReturnValue(mockReturnValue);
        expect(utils.getCanvasFromHTML()).toBe(mockReturnValue);
    });

    test("it should return error if canvas is not found from HTML", () => {
        // tslint:disable-next-line
        document.getElementById = jest.fn().mockReturnValue(null);
        expect(() => utils.getCanvasFromHTML()).toThrowError(ErrorMessage.CanvasNotFound);
    });
});

describe("setObject", () => {
    test("it should set given properties to the given object", () => {
        const mockObject = { hello: "world" };
        const mockProps = { world: "hello" };
        utils.setObject(mockObject, mockProps);
        expect(Object.entries(mockObject).some(
            ([key, value]) => key === "world" && value === "hello"
        ))
            .toBe(true);
    });
});

describe("setContext", () => {
    test("it should call setObject", () => {
        const mockContext = {} as unknown as CanvasRenderingContext2D;
        const mockProps = {};
        utils.setContext(mockContext, mockProps);
    });

    test("it should set given properties to the given object", () => {
        const mockObject = { hello: "world" } as unknown as CanvasRenderingContext2D;
        const mockProps = { world: "hello" };
        utils.setContext(mockObject, mockProps);
        expect(Object.entries(mockObject).some(
            ([key, value]) => key === "world" && value === "hello"
        ))
            .toBe(true);
    });
});

describe("getTimeDifference", () => {
    test("returns time difference between two dates", () => {
        const mockReturnValue1 = 10;
        const mockDate1 = { getTime: () => mockReturnValue1 } as unknown as Date;
        const mockReturnValue2 = 25;
        const mockDate2 = { getTime: () => mockReturnValue2 } as unknown as Date;
        const mockReturnValue = mockReturnValue2 - mockReturnValue1;
        expect(utils.getTimeDifference(mockDate1, mockDate2)).toBe(mockReturnValue);
    });
});
