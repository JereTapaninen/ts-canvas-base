import app from "../src/app";
import { Canvas, Context } from "../src/types";

const fillRectangle = jest.fn();
const fillText = jest.fn();

const mockContext = {
    fillRectangle,
    fillText
} as unknown as Context;

const getContext = jest.fn().mockReturnValue(mockContext);
const setSize = jest.fn();
const getBounds = jest.fn().mockReturnValue({
    left: 0,
    top: 0
});
const getSize = jest.fn().mockReturnValue({
    width: 0,
    height: 0
});

const mockCanvas = {
    getContext,
    setSize,
    getBounds,
    getSize
} as unknown as Canvas;

beforeEach(() => {
    jest.clearAllMocks();
});

describe("app", () => {
    test("it returns a function", () => {
        expect(typeof app(mockCanvas)).toBe("function");
    });

    test("it calls setObject", () => {
        const utils = require("../src/utils");
        // tslint:disable-next-line
        utils.setObject = jest.fn();
        app(mockCanvas);
        expect(utils.setObject).toBeCalledTimes(1);
    });
});

describe("animationLoop", () => {
    test("it calls setSize on canvas", () => {
        const animationLoop = app(mockCanvas);
        animationLoop();
        expect(setSize).toBeCalledTimes(1);
    });

    test("it calls requestAnimationFrame", () => {
        // tslint:disable-next-line
        window.requestAnimationFrame = jest.fn();

        const animationLoop = app(mockCanvas);
        animationLoop();
        expect(window.requestAnimationFrame).toBeCalledTimes(1);
    });
});
