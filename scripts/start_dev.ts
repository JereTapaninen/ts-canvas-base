"use strict";

const captureExit = require("capture-exit");
const { spawn } = require("child_process");

const runServe = spawn("npm", ["run", "serve"], { cwd: "./", shell: true });
const runWatch = spawn("npm", ["run", "watch"], { cwd: "./", shell: true });

const onDataOrError = (data: Buffer) => {
    const str = data.toString("utf-8");
    const lines = str.split(/(\r?\n)/g).filter(line => line !== "" && line !== '\n');
    lines.forEach(line => { console.log(line); })
};

runServe.stdout.on("data", onDataOrError);
runWatch.stdout.on("data", onDataOrError);

runServe.stderr.on("data", onDataOrError);
runWatch.stderr.on("data", onDataOrError);

runServe.on("close", (code: number) => {
    console.log(`Serve process closed with code ${code}`);
});

runWatch.on("close", (code: number) => {
    console.log(`Watch process closed with code ${code}`);
});

const onExit = () => {
    console.log("Exiting...");
    runServe.kill();
    runWatch.kill();
};

process.on("SIGINT", onExit);

captureExit.captureExit();
captureExit.onExit(onExit);
