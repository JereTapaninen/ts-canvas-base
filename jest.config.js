module.exports = {
    "displayName": "ts-canvas",
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
    "testRegex": "test/.*/*.*.ts",
    "modulePathIgnorePatterns": [
        "<rootDir>/.*/__mocks__",
        "mocks.ts"
    ],
    "collectCoverageFrom": [
        "src/**/*.ts",
        "!src/index.ts"
    ],
    "coverageThreshold": {
        "global": {
            "branches": 100,
            "functions": 100,
            "lines": 100,
            "statements": 100
        }
    },
    "globals": {
        "ts-jest": {
            "tsConfig": "tsconfig.json"
        }
    }
};
