import { Context, Rectangle, Point } from "../types";
import { FillStyle, Font } from "../constants";
import { setContext } from "../utils";

export default (htmlContext: CanvasRenderingContext2D) => (): Context => ({
    fillRectangle: (style: FillStyle, { x, y, width, height }: Rectangle) => {
        setContext(htmlContext, { fillStyle: style });
        htmlContext.fillRect(x, y, width, height);
    },
    fillText: (style: FillStyle, font: Font, text: string, { x, y }: Point) => {
        setContext(htmlContext, { font, fillStyle: style });
        htmlContext.fillText(text, x, y);
    }
});
