import { Point, Size, Rectangle } from "../types";
import { ErrorMessage, Types } from "../constants";
import { trueOrThrow, objectContains } from "../utils";

const createRectangleWithPointAndSize = (point: Point, size: Size): Rectangle => ({
    x: point.x,
    y: point.y,
    width: size.width,
    height: size.height
});

const createRectangle = (x: number, y: number, width: number, height: number): Rectangle => ({
    x, y, width, height
});

export default (
    xOrPoint: number | Point,
    yOrSize: number | Size,
    width?: number,
    height?: number
): Rectangle => {
    const calledWithPointAndSize: boolean = (
        typeof xOrPoint === Types.Object &&
        objectContains(xOrPoint as object, "x", "y") &&
        typeof yOrSize === Types.Object &&
        objectContains(yOrSize as object, "width", "height")
    );
    const calledWithXYWidthHeight: boolean = (
        typeof xOrPoint === Types.Number &&
        typeof yOrSize === Types.Number &&
        typeof width === Types.Number &&
        typeof height === Types.Number
    );

    return calledWithPointAndSize ?
        createRectangleWithPointAndSize(xOrPoint as Point, yOrSize as Size) :
        trueOrThrow(
            calledWithXYWidthHeight,
            () => createRectangle(
                xOrPoint as number,
                yOrSize as number,
                width as number,
                height as number
            ),
            ErrorMessage.InvalidRectangleArguments
        );
};
