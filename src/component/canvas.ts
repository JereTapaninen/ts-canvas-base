import { setObject, tryDo } from "../utils";
import { Size, Canvas, Context } from "../types";
import { ErrorMessage } from "../constants";
import createContext from "./context";

export default (htmlCanvas: HTMLCanvasElement) => (): Canvas => ({
    setSize: ({width, height}: Size) => {
        setObject(htmlCanvas, { width, height });
    },
    getSize: (): Size =>
        ({ width: htmlCanvas.width, height: htmlCanvas.height }),
    getContext: (): Context =>
        createContext(tryDo<CanvasRenderingContext2D>(
            htmlCanvas.getContext("2d"),
            ErrorMessage.CouldNotGetContext
        ))(),
    getBounds: (): ClientRect | DOMRect =>
        htmlCanvas.getBoundingClientRect(),
});
