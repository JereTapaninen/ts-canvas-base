import { FillStyle, Font } from "./constants";

export interface AppState {
    _canvas: Canvas | null;
    canvas: Canvas;
    _context: Context | null;
    context: Context;
    canvasBounds: Rectangle;
}

export interface FpsState {
    counter: number;
    lastTime: Date;
    fps: number;
}

export interface Size {
    width: number;
    height: number;
}

export interface Point {
    x: number;
    y: number;
}

export interface Rectangle {
    x: number;
    y: number;
    width: number;
    height: number;
}

export interface Canvas {
    setSize: (size: Size) => void;
    getSize: () => Size;
    getContext: () => Context;
    getBounds: () => ClientRect | DOMRect;
}

export interface Context {
    fillRectangle: (style: FillStyle, bounds: Rectangle) => void;
    fillText: (style: FillStyle, font: Font, text: string, position: Point) => void;
}
