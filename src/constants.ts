import { Point } from "./types";

export const ErrorMessage = {
    State: {
        NullCanvas: "Canvas is null in state",
        NullContext: "Canvas Context is null in state"
    },
    InvalidRectangleArguments: "Tried to create rectangle with invalid arguments.",
    CouldNotGetContext: "Could not get context from canvas",
    CanvasNotFound: "Canvas not found from HTML!",
    Default: "Unexpected error occurred."
};

export const ElementId = {
    Canvas: "canvas"
};

export const FpsPosition: Point = {
    x: 10,
    y: 30
};

export enum FillStyle {
    FPS = "yellow",
    Background = "black"
}

export enum Font {
    FPS = "24px Roboto"
}

export const Types = {
    Number: "number",
    Object: "object"
};

// Helper because date is ms based
export const TimeSpan = {
    Second: 1000
};
