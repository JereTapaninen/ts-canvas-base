import { FpsState, Context } from "../types";
import { FpsPosition, FillStyle, Font, TimeSpan } from "../constants";
import { getTimeDifference, doIfTrue, setObject } from "../utils";

const state: FpsState = {
    counter: 0,
    lastTime: new Date(),
    fps: 0
};

const setFps = () => {
    setObject(state, {
        fps: state.counter,
        counter: 0,
        lastTime: new Date()
    });
};

const countFps = () => {
    setObject(state, { counter: state.counter + 1 });

    const secondElapsed = getTimeDifference(state.lastTime, new Date()) >= TimeSpan.Second;
    doIfTrue(secondElapsed, setFps);
};

export default (ctx: Context) => {
    countFps();

    ctx.fillText(FillStyle.FPS, Font.FPS, `FPS: ${state.fps}`, FpsPosition);
};

export const forTests = {
    setFps
};
