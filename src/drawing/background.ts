import { Rectangle, Context } from "../types";
import { FillStyle } from "../constants";

export default (ctx: Context, bounds: Rectangle) => {
    ctx.fillRectangle(FillStyle.Background, bounds);
};
