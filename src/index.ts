import { getCanvasFromHTML } from "./utils";
import drawLoop from "./app";
import { Canvas } from "./types";
import createCanvas from "./component/canvas";

(() => {
    const htmlCanvas: HTMLCanvasElement = getCanvasFromHTML();
    const canvas: Canvas = createCanvas(htmlCanvas)();

    window.requestAnimationFrame(drawLoop(canvas));
})();
