import { AppState, Canvas, Context } from "./types";
import { tryDo, setObject } from "./utils";
import { ErrorMessage } from "./constants";
import createRectangle from "./component/rectangle";
import drawBackground from "./drawing/background";
import drawFps from "./drawing/fps";

const state: AppState = {
    _canvas: null,
    get canvas() {
        return tryDo<Canvas>(state._canvas, ErrorMessage.State.NullCanvas);
    },
    _context: null,
    get context() {
        return tryDo<Context>(state._context, ErrorMessage.State.NullContext);
    },
    get canvasBounds() {
        const { left: x, top: y } = state.canvas.getBounds();
        const { width, height } = state.canvas.getSize();
        return createRectangle(x, y, width, height);
    }
};

const animationLoop = (): void => {
    state.canvas.setSize({ width: window.innerWidth, height: window.innerHeight });

    drawBackground(state.context, state.canvasBounds);
    drawFps(state.context);

    window.requestAnimationFrame(animationLoop);
};

export default (canvas: Canvas) => {
    setObject(state, {
        _canvas: canvas,
        _context: canvas.getContext()
    });
    return animationLoop;
};
