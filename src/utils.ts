import { ErrorMessage, ElementId } from "./constants";

// This is the utils class, where we hide all the dirty mutation stuff for lulz
// tslint:disable:no-if-statement no-throw no-object-mutation

export const tryDo = <T>(
    action: any,
    errorMsg: string = ErrorMessage.Default
): T => {
    if (action === null || action === undefined) {
        throw Error(errorMsg);
    }

    return action;
};

export const orThrow = <T>(
    check1: any,
    check2: any,
    getReturnValue: () => T,
    errorMsg: string = ErrorMessage.Default
): T => {
    if (check2 !== check1) {
        throw Error(errorMsg);
    }

    return getReturnValue();
};

export const trueOrThrow = <T>(
    boolCheck: boolean,
    getReturnValue: () => T,
    errorMsg: string = ErrorMessage.Default
): T =>
    orThrow(true, boolCheck, getReturnValue, errorMsg);

export const doIf = (
    check1: any,
    check2: any,
    action: () => void
): void => {
    if (check2 === check1) {
        action();
    }
};

export const doIfTrue = (
    boolCheck: boolean,
    action: () => void
): void => {
    doIf(true, boolCheck, action);
};

export const objectContains = (
    obj: object,
    ...keys: ReadonlyArray<string>
): boolean =>
    keys.every(key => key in obj);

export const getCanvasFromHTML = (): HTMLCanvasElement => {
    return tryDo<HTMLCanvasElement>(
        document.getElementById(ElementId.Canvas),
        ErrorMessage.CanvasNotFound
    );
};

export const setObject = (stateObject: object, props: object): void => {
    Object.assign(stateObject, props);
};

export const setContext = (ctx: CanvasRenderingContext2D, props: object): void =>
    setObject(ctx, props);

export const getTimeDifference = (earlier: Date, later: Date): number =>
    later.getTime() - earlier.getTime();
